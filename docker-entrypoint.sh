#!/bin/bash
set -e

mkdir -p "$CONF_HOME";
chmod 700 "$CONF_HOME";
chown -R bitbucket: "$CONF_HOME";

exec gosu bitbucket "${CONF_INSTALL}/bin/start-bitbucket.sh" "-fg";

exec "$@"
